//
//  ViewController.swift
//  ScoreTrackerSwift
//
//  Created by Garrick McMickell on 9/18/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var step1: UIStepper!
    @IBOutlet weak var step2: UIStepper!
    @IBOutlet weak var btn: UIButton!
    
    @IBAction func step1Changed(sender: UIStepper) {
        lbl1.text = String(format: "%.f", step1.value)
    }
    
    @IBAction func step2Changed(sender: UIStepper) {
        lbl2.text = String(format: "%.f", step2.value)
    }
    
    @IBAction func btnClicked(sender: UIButton) {
        lbl1.text = "0"
        step1.value = 0
        lbl2.text = "0"
        step2.value = 0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl1.layer.masksToBounds = true
        lbl1.layer.cornerRadius = 5
        lbl2.layer.masksToBounds = true
        lbl2.layer.cornerRadius = 5
        step1.layer.cornerRadius = 5
        step2.layer.cornerRadius = 5
        btn.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

